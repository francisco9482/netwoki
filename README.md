# netwoki

## La vulnerabilidad se refiere a un problema conocido dentro de un sistema o programa. 

Un ejemplo común en InfoSec se llama desbordamiento del búfer o vulnerabilidad por desbordamiento del búfer. 

Los programadores tienden a confiar y no preocuparse por quién atacará sus programas, sino por quién usará sus programas legítimamente. Una característica de la mayoría de los programas es la capacidad de un usuario de "ingresar" información o solicitudes. Las instrucciones del programa (código fuente) contienen un “área” en la memoria (búfer) para estas entradas y actúan sobre ellas cuando se les pide que lo hagan. 

A veces, el programador no comprueba si la entrada es correcta o inocua. Un usuario malintencionado, sin embargo, podría aprovechar esta debilidad y sobrecargar el área de entrada con más información de la que puede manejar, bloqueando o deshabilitando el programa. Esto se denomina desbordamiento de búfer y puede permitir que un usuario malintencionado obtenga el control del sistema. Esta vulnerabilidad común con el software debe abordarse al desarrollar sistemas. 

Un exploit es un programa o "libro de cocina" sobre cómo aprovechar una vulnerabilidad específica. Puede ser un programa que un pirata informático puede descargar de Internet y luego usar para buscar sistemas que contengan la vulnerabilidad para la que está diseñado. También puede ser una serie de pasos documentados sobre cómo aprovechar la vulnerabilidad después de que un atacante encuentra un sistema que la contiene.

Un atacante, entonces, es el vínculo entre una vulnerabilidad y un exploit. El atacante tiene dos características: habilidad y voluntad. Los atacantes son expertos en el arte de atacar sistemas o tienen acceso a herramientas que hacen el trabajo por ellos. Tienen la voluntad de realizar ataques a sistemas que no son de su propiedad y, por lo general, se preocupan poco por las consecuencias de sus acciones.

Al aplicar estos conceptos al análisis de riesgos, el profesional de SI debe anticipar quién podría querer atacar el sistema, qué tan capaz podría ser el atacante, qué tan disponibles están los exploits para una vulnerabilidad y qué sistemas tienen la vulnerabilidad presente.

El análisis y la gestión de riesgos en los [servicios de mensajería](https://bitcu.co/whatsapp-web/) son áreas especializadas de estudio y práctica, y los profesionales de SI que se concentran en estas áreas deben estar capacitados y actualizados en sus técnicas.

